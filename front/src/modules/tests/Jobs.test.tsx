import { ReactElement } from 'react';
import { render, screen, fireEvent, waitFor } from '@testing-library/react';
import { QueryClient, QueryClientProvider } from 'react-query';
import Jobs from '../Jobs';
import useApiQuery from '../../services/api';

jest.mock('../../services/api', () => ({
  __esModule: true,
  default: jest.fn(),
}));

jest.mock('react-query', () => ({
  ...jest.requireActual('react-query'),
  useMutation: jest.fn(),
}));

describe('Jobs component', () => {
  const queryClient = new QueryClient();

  beforeEach(() => {
    jest.clearAllMocks();
  });

  const renderWithQueryClient = (ui: ReactElement) => {
    return render(
      <QueryClientProvider client={queryClient}>{ui}</QueryClientProvider>
    );
  };

  it('displays the list of jobs', async () => {
    const jobsData = [
      { id: 1, task: 'Job 1' },
      { id: 2, task: 'Job 2' },
    ];

    (useApiQuery as jest.Mock).mockImplementation((key: string) => {
      if (key === 'jobs') {
        return {
          data: jobsData,
          error: null,
          isLoading: false,
        };
      }
      return {};
    });

    renderWithQueryClient(<Jobs />);

    expect(screen.getByText('Job 1')).toBeInTheDocument();
    expect(screen.getByText('Job 2')).toBeInTheDocument();
  });

  it('displays job details when a job is clicked', async () => {
    const jobsData = [{ id: 1, task: 'Job 1' }];
    const jobDetails = {
      id: 1,
      task: 'Job 1',
      status: 'Pending',
      user: 'User 1',
      group: 'Group 1',
      dateSubmitted: '2024-06-14',
      dateComplete: null,
    };

    (useApiQuery as jest.Mock).mockImplementation((key: string) => {
      if (key === 'jobs') {
        return {
          data: jobsData,
          error: null,
          isLoading: false,
        };
      }
      if (key === 'job-1') {
        return {
          data: jobDetails,
          error: null,
          isLoading: false,
        };
      }
      return {};
    });

    renderWithQueryClient(<Jobs />);

    fireEvent.click(screen.getByText('Job 1'));
    await waitFor(() => {
      expect(screen.getByText('Selected Job Details')).toBeInTheDocument();
      expect(screen.getByText('ID: 1')).toBeInTheDocument();
      expect(screen.getByText('Status: Pending')).toBeInTheDocument();
      expect(screen.getByText('User: User 1')).toBeInTheDocument();
      expect(screen.getByText('Group: Group 1')).toBeInTheDocument();
      expect(
        screen.getByText('Date Submitted: 2024-06-14')
      ).toBeInTheDocument();
      expect(
        screen.getByText('Date Complete: Not available')
      ).toBeInTheDocument();
    });
  });

  it('should open the CreateNew modal when "Create job" is clicked', () => {
    renderWithQueryClient(<Jobs />);

    expect(screen.queryByText('Create a new task')).not.toBeInTheDocument();

    fireEvent.click(screen.getByText('Create job'));

    expect(screen.getByText('Create a new task')).toBeInTheDocument();
  });
});
