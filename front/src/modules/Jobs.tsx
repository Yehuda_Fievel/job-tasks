import { Button } from 'antd';
import { CreateNew } from './components/CreateNew';
import { SelectedJob } from './components/SelectedJob';
import { useGetJobById, useGetJobs, useJobHooks } from './hooks';
import { Job } from '../types';
import './jobs.scss';

export default function Jobs() {
  const { data, error, isLoading } = useGetJobs();

  const {
    selectedJobId,
    handleJobClick,
    isCreateNewModal,
    setIsCreateNewModal,
    onClickCreate,
  } = useJobHooks();

  const {
    data: selectedJob,
    error: selectedJobError,
    isLoading: isLoadingSelectedJob,
  } = useGetJobById(selectedJobId ?? -1);

  if (isLoading) return <div>Loading...</div>;
  if (error) return <div>An error occurred: {error.message}</div>;
  return (
    <div className="jobs-container">
      <div>
        <Button type="primary" onClick={onClickCreate}>
          Create job
        </Button>
      </div>
      <CreateNew
        isModalOpen={isCreateNewModal}
        setIsModal={setIsCreateNewModal}
      />
      {data?.map((item: Job) => (
        <div key={item.id}>
          <div className="job-item" onClick={() => handleJobClick(item.id)}>
            <div className="task-label">Tasks:</div>
            <div className="job-details">
              <div className="job-name">{item.task}</div>
            </div>
          </div>
          {selectedJobId === item.id &&
            !isLoadingSelectedJob &&
            !selectedJobError &&
            selectedJob && <SelectedJob selectedJob={selectedJob} />}

          {selectedJobId === item.id && isLoadingSelectedJob && (
            <div className="job-details-expanded">Loading job details...</div>
          )}
          {selectedJobId === item.id && selectedJobError && (
            <div className="job-details-expanded">
              An error occurred while loading job details.
            </div>
          )}
        </div>
      ))}
    </div>
  );
}
