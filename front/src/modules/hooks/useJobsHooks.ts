import { useState } from 'react';

export const useJobHooks = () => {
  const [selectedJobId, setSelectedJobId] = useState<number | null>(null);
  const [isCreateNewModal, setIsCreateNewModal] = useState(false);

  const handleJobClick = (jobId: number) => {
    setSelectedJobId((prevSelectedJobId) =>
      prevSelectedJobId === jobId ? null : jobId
    );
  };

  const onClickCreate = () => setIsCreateNewModal(true);

  return {
    selectedJobId,
    handleJobClick,
    isCreateNewModal,
    setIsCreateNewModal,
    onClickCreate,
  };
};
