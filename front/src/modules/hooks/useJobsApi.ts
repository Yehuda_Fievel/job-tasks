import useApiQuery from '../../services/api';
import { Job } from '../../types';

export const useGetJobs = () => {
  const { data, error, isLoading } = useApiQuery<Job[]>('jobs', {
    method: 'GET',
    url: '/jobs',
  });
  return { data, error, isLoading };
};

export const useGetJobById = (id: number) => {
  const { data, error, isLoading } = useApiQuery<Job>(`job-${id}`, {
    method: 'GET',
    url: `/jobs/${id}`,
  });
  return { data, error, isLoading };
};