import { Form, Input, Modal } from 'antd';
import { useCreateNew } from './useCreateNew';

interface Props {
  isModalOpen: boolean;
  setIsModal: (bool: boolean) => void;
}

export const CreateNew = ({ isModalOpen, setIsModal }: Props) => {
  const { form, handleOk, handleCancel } = useCreateNew({ setIsModal });

  return (
    <>
      <Modal
        title="Create a new task"
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
      >
        <Form form={form} name="create-job">
          <Form.Item name="task" label="Task" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="user" label="User" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item name="group" label="Group" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};
