import { Form } from 'antd';
import { useMutation, useQueryClient } from 'react-query';
import { makeRequest } from '../../../services/api';
import { Job } from '../../../types';

interface Params {
  setIsModal: (bool: boolean) => void;
}

export const useCreateNew = ({ setIsModal }: Params) => {
  const [form] = Form.useForm();

  const queryClient = useQueryClient();

  const mutation = useMutation(
    (formData: object) => {
      return makeRequest({
        method: 'POST',
        url: '/jobs',
        body: formData,
      });
    },
    {
      onSuccess: (data) => {
        queryClient.setQueryData<Job[]>('jobs', (prevJobs) => [
          ...(prevJobs ?? []),
          data,
        ]);
        setIsModal(false);
      },
      onError: (error) => {
        console.error('Failed to create task:', error);
      },
    }
  );

  const handleOk = async () => {
    try {
      const values = await form.validateFields();
      await mutation.mutateAsync(values);
      form.resetFields();
    } catch (error) {
      console.error('Validate Failed:', error);
    }
  };

  const handleCancel = () => {
    form.resetFields();
    setIsModal(false);
  };

  return { form, handleOk, handleCancel };
};
