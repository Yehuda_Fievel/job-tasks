import { Job } from '../../../types';

interface Props {
  selectedJob: Job;
}

export const SelectedJob = ({ selectedJob }: Props) => {
  return (
    <div className="job-details-expanded">
      <h3>Selected Job Details</h3>
      <p>ID: {selectedJob.id}</p>
      <p>Status: {selectedJob.status}</p>
      <p>User: {selectedJob.user}</p>
      <p>Group: {selectedJob.group}</p>
      <p>Date Submitted: {selectedJob.dateSubmitted}</p>
      <p>Date Complete: {selectedJob.dateComplete || 'Not available'}</p>
    </div>
  );
};
