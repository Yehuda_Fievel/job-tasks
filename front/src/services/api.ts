import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';
import { useQuery } from 'react-query';
import { SERVER_URL } from '../config';

type HttpMethod = 'GET' | 'POST' | 'PUT' | 'DELETE';

interface Params {
  method: HttpMethod;
  url: string;
  baseUrl?: string;
  body?: object;
}

export default function useApiQuery<T>(key: string, params: Params) {
  return useQuery<T, Error>(key, () => makeRequest(params));
}

export async function makeRequest({ method, baseUrl, url, body }: Params) {
  const config: AxiosRequestConfig = {
    method,
    url,
    baseURL: baseUrl || SERVER_URL,
    ...(body && { data: body }),
  };

  try {
    const response: AxiosResponse = await axios(config);
    return response.data;
  } catch (error) {
    if (axios.isAxiosError(error) && error.response) {
      throw new Error(error.response.data.message || 'An error occurred');
    }
    throw error;
  }
}
