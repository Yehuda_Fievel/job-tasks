export interface Job {
  id: number;
  task: string;
  status: JobStatus;
  user: string;
  group: string;
  dateSubmitted: string;
  dateComplete: string | null;
}

export type JobStatus =
  | 'pending'
  | 'running'
  | 'failed'
  | 'succeeded'
  | 'terminated';
