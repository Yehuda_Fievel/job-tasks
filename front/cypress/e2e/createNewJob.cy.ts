describe('Create New Job', () => {
  const url = 'http://localhost:4000';

  beforeEach(() => {
    cy.intercept('GET', `${url}/jobs`, { fixture: 'jobs.json' }).as('getJobs');
    cy.visit('/');
  });

  it('should create a new job successfully', () => {
    cy.wait('@getJobs');

    cy.contains('Create job').click();
    cy.contains('Create a new task').should('be.visible');

    cy.get('input#create-job_task').type('New Job Task');
    cy.get('input#create-job_user').type('User1');
    cy.get('input#create-job_group').type('Group1');

    cy.intercept('POST', `${url}/jobs`, {
      statusCode: 201,
      body: { id: 3, task: 'New Job Task', user: 'User1', group: 'Group1' },
    }).as('createJob');

    cy.contains('OK').click();

    cy.wait('@createJob').then((interception) => {
      if (interception && interception.response) {
        expect(interception.response.statusCode).to.equal(201);
        expect(interception.response.body).to.deep.equal({
          id: 3,
          task: 'New Job Task',
          user: 'User1',
          group: 'Group1',
        });
      } else {
        throw new Error(
          'Create job request was not intercepted or response is undefined'
        );
      }
    });

    cy.contains('New Job Task').should('be.visible');
  });
});
