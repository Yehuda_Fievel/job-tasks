describe('Get Job By ID', () => {
  const url = 'http://localhost:4000';
  beforeEach(() => {
    cy.intercept('GET', `${url}/jobs`, { fixture: 'jobs.json' }).as('getJobs');
    cy.intercept('GET', `${url}/jobs/1`, { fixture: 'job1.json' }).as(
      'getJobById'
    );
    cy.visit('/');
  });

  it('should display job details when a job is clicked', () => {
    cy.wait('@getJobs');
    cy.get('.job-item').first().click();
    cy.wait('@getJobById');

    cy.contains('Selected Job Details').should('be.visible');
    cy.contains('ID: 1').should('be.visible');
    cy.contains('Status: Completed').should('be.visible');
  });
});
