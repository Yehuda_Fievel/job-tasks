describe('Get All Jobs', () => {
  beforeEach(() => {
    cy.intercept('GET', 'http://localhost:4000/jobs', {
      fixture: 'jobs.json',
    }).as('getJobs');
    cy.visit('/');
  });

  it('should display a list of jobs', () => {
    cy.wait('@getJobs');
    cy.get('.job-item').should('have.length', 2);
    cy.get('.job-item').first().should('contain', 'Job 1');
  });
});
