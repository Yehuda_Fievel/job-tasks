# Server

## How to run to the server

- docker build -t server <path/to/server>
- docker run -p 4000:4000 server

# Front

## How to run the front

- docker build -t react-app <path/to/front>
- docker run -p 4173:4173 react-app
- navigate to http://172.17.0.3:4173/

# Running the application

- Opening the website displays a list of jobs
- Clicking on a item expands the list to see the job details
- Clicking on the button on the top of the page opens a modal to create a new job that will be added to the list