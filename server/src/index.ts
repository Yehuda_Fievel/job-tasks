import express from 'express';
import cors from 'cors';
import router from './entities';
import { runJobs } from './services/jobService';

// If I had more time I would use dotenv and create error handling

const app = express();
const port = 4000;

app.use(cors());
app.use(express.json());

app.use((req, res, next) => {
  console.log(`${req.method} ${req.url}`);
  next();
});

router(app);

app.use((req, res) => {
  console.log(404);
});

runJobs();

app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
