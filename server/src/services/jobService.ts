import fs from 'fs';

const JOB_BATCH = 3;
const JOB_INTERVAL = 30000;

export const runJobs = () => {
  setInterval(function () {
    const jobs = JSON.parse(fs.readFileSync('jobs.json', 'utf8'));
    if (!jobs?.length) {
      return;
    }
    let incompleteJobsCount = 0;

    for (const job of jobs) {
      if (job.status === 'pending') {
        job.status = 'running';
        fs.writeFileSync('jobs.json', JSON.stringify(jobs, null, 2));

        // Simulate running the job
        console.log(`Running job: ${job.task}`);

        // update the job if it's failed, succeeded or terminated
        // for this example I will mark them as completed
        job.status = 'succeeded';
        fs.writeFileSync('jobs.json', JSON.stringify(jobs, null, 2));

        incompleteJobsCount++;
        if (incompleteJobsCount === JOB_BATCH) {
          break;
        }
      }
    }

    fs.writeFileSync('jobs.json', JSON.stringify(jobs, null, 2));
  }, JOB_INTERVAL);
};
