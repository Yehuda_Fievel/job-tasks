import { Router } from 'express';
import { getJobById, getJobs, postJob } from './jobsController';

const router = Router();

router.post('', postJob);

router.get('', getJobs);

router.get('/:id', getJobById);

export default router;
