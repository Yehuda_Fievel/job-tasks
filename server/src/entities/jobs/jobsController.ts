import { Request, Response, NextFunction } from 'express';
import fs from 'fs';

export const postJob = (req: Request, res: Response, next: NextFunction) => {
  console.log('post job')
  const jobs = JSON.parse(fs.readFileSync('jobs.json', 'utf8'));
  const job = {
    id: jobs.length,
    status: 'pending',
    ...req.body,
    dateSubmitted: new Date().toISOString(),
  };
  jobs.push(job);
  console.log(job);
  fs.writeFileSync('jobs.json', JSON.stringify(jobs, null, 2));
  res.status(201).json(job);
};

export const getJobs = (req: Request, res: Response, next: NextFunction) => {
    console.log('getJob');
  // if I had more time I would add server-side pagination
  const jobs = JSON.parse(fs.readFileSync('jobs.json', 'utf8'));
  res.status(200).json(jobs);
};

export const getJobById = (req: Request, res: Response, next: NextFunction) => {
      console.log('getJobs');
const jobs = JSON.parse(fs.readFileSync('jobs.json', 'utf8'));
  const job = jobs.find((job: { id: number }) => job.id == +req.params.id);
  res.status(200).json(job);
};
