import { Application } from 'express';
import jobRouter from './jobs/jobRoutes';

export default (app: Application) => {
  app.use('/jobs', jobRouter);
};
